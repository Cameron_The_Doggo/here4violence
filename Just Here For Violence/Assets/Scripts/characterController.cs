﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class characterController : MonoBehaviour
{
    [SerializeField] private float Speed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float forwards = Input.GetAxis("Vertical") * Speed;
        float sideways = Input.GetAxis("Horizontal") * Speed;

        forwards *= Time.deltaTime;
        sideways *= Time.deltaTime;

        transform.Translate(sideways, 0, forwards);

        if (Input.GetKeyDown("escape")) Cursor.lockState = CursorLockMode.None;
    }
}
